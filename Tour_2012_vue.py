# -*- coding: ISO-8859-1 -*-
'''
@author: Maxime Cot�, Jean-Philippe Parent, Marc-Andr� De Gagn�, Nicolas Waucheul, Shi-Hui Tran

'''

from tkinter import *
import random

class Vue():
    def __init__(self, parent):
        self.parent=parent
        self.root=Tk()
        self.root.title("Feast Of Creeps")
        self.mainFrame=Frame(width=600,height=800)
        self.status=1 
        self.statusbutton={1:ACTIVE,0:DISABLED}
        self.debug=Debuger(self)
        self.canevas=Canvas(self.mainFrame,highlightthickness=0,width=500,height=500)
        self.canevas.bind("<Button-1>",self.getPosTour)
        self.couleurCreep="red"
        self.couleurVie="black" 
        self.cash=0
        self.argent=StringVar()        
        self.nivo=StringVar()
        self.vague=StringVar()
        self.vie=StringVar()
        self.canevasCree=0
        self.afficherbuttonPause()
        self.canevas.focus_set()
        
        #image des tours
        self.imageTourFaible=PhotoImage(file="images/grasstower.gif")
        self.imageTourMoyenne=PhotoImage(file="images/magictower.gif")
        self.imageTourForte=PhotoImage(file="images/firetower.gif")     

        self.afficherMenuArgent()
        self.afficherMenuPouvoir()
        self.afficherMenuTour()
        self.afficherBouttonDemarrer()
        self.mainFrame.pack()
        self.frameDemarrer.grid(row=0,column=0,sticky=EW)
        self.frameArgent.grid(row=1,column=0,sticky=EW)
        self.canevas.grid(row=2,column=0)
        self.framePouvoir.grid(row=3,column=0,sticky=EW)
        self.frameTour.grid(row=1,column=1,rowspan=2,sticky=NS)
     
     
      
          
    
         
           
    def redessinerJeu(self):
        self.root.destroy()   
     
    def affichermenuPause(self):
        self.frameDemarrer.grid_forget()
        self.frameDemarrer.destroy()
        self.framePause.grid(row=0,column=0,sticky=EW)  
     
    def selectClik(self,type):
        if type==1:
            self.canevas.bind("<Button-1>",self.getPosTour)
        else:
            self.canevas.bind("<Button-1>",self.upgradeTour)
 
    def upgradeTour(self,evt):
        x=int(evt.x/5)
        y=int(evt.y/5)
        self.parent.upgradeTour([x,y])      
        
        
    def getPosTour(self,evt):
        x=evt.x
        y=evt.y
        #retourne les corrdonn�es du clique dans le canevas
        self.parent.setTour([x,y])
        
        
    
    def dessineTour(self,pos,type):
        #r�ajustement des coordonn�es pour travailler dans le canevas 500x500 
        #(devrait �tre dasn le modele mais code le permet pas vraiment pour l'instant)
        x1=pos[0]*5
        y1=pos[1]*5
        x2=pos[0]*5+5
        y2=pos[1]*5+5
        
        if type == 1:                
            self.canevas.create_image(x1,y1,image=self.imageTourFaible, tags="tour")
            #self.canevas.create_rectangle(x1,y1,x2,y2, width=1, fill="green", tags=("tour",), activefill="pink") #activefill pour tester, si on en a besoin
        if type == 2:
            self.canevas.create_image(x1,y1,image=self.imageTourMoyenne, tags="tour")
            #self.canevas.create_rectangle(x1,y1,x2,y2, width=1, fill="yellow", tags=("tour",))
        if type == 3:
            self.canevas.create_image(x1,y1,image=self.imageTourForte, tags="tour")
            #self.canevas.create_rectangle(x1,y1,x2,y2, width=1, fill="red", tags=("tour",))
             
            
     
    def choisirPouvoir(self,type):
        self.parent.choisirTypePouvoir(type)        
        
    def choisirTour(self,type):
        self.selectClik(1)
        self.parent.choisirTypeTour(type)
        
    def typeUpgrade(self,type):
        self.selectClik(2)
        self.parent.typeUpgrade(type)

        
        
    def afficheModele(self):
        #images utilis�es pour la carte
        #carte avec obstacles inclus         
        #self.background_image=PhotoImage(file="images/FoC_map1_FINALv2_.gif")  
        #carte d�pourvue d'obstacles
        self.background_image=PhotoImage(file="images/FoC_map1_vide_v2.gif") 
        #obstacles pour la carte, roches, arbres, etc  
        self.sapin1=PhotoImage(file="images/connifere1v2.gif")  
        self.sapin2=PhotoImage(file="images/connifere2.gif")   
        self.roche1=PhotoImage(file="images/roche1.gif")   
        self.roche2=PhotoImage(file="images/roche2.gif") 
        self.roche3=PhotoImage(file="images/grandeRoche.gif")   
           
        
        #application des images
        self.canevas.create_image(0,0,anchor=NW,image=self.background_image, tags="carte")
        self.canevas.create_image(180,85, image=self.sapin1, tags=("obstacle","sapin1"))
        self.canevas.create_image(335,200, image=self.sapin2, tags=("obstacle","sapin2"))
        self.canevas.create_image(70,285, image=self.sapin1, tags=("obstacle","sapin3"))
        self.canevas.create_image(300,450, image=self.sapin2, tags=("obstacle","sapin4"))
        self.canevas.create_image(65,415, image=self.roche1, tags=("obstacle","roche1"))
        self.canevas.create_image(375,40, image=self.roche2, tags=("obstacle","roche2"))
        self.canevas.create_image(465,100, image=self.roche3, tags=("obstacle","roche3"))
                
        pos=[]
        for i in self.parent.modele.nivoActif.parcours.noeuds:
            pos.append(i[0]*5)
            pos.append(i[1]*5)
    
    def activerPouvoirs(self):
        if self.parent.modele.nivo >1:                                                      #"JAI DETESTER FAIRE CETTE PARTIE" -nico
            self.b1.config(state=NORMAL,text=str(self.parent.modele.cout["Glace"])+"$")
        if self.parent.modele.nivo >2:
            self.b2.config(state=NORMAL,text=str(self.parent.modele.cout["Slash"])+"$")
        if self.parent.modele.nivo >3:
            self.b3.config(state=NORMAL,text=str(self.parent.modele.cout["Vent"])+"$")
        if self.parent.modele.nivo >4:
            self.b4.config(state=NORMAL,text=str(self.parent.modele.cout["Nuke"])+"$")
            self.b5.config(state=NORMAL,text=str(self.parent.modele.cout["Heal"])+"$")


    def afficherMenuPouvoir(self):
        self.framePouvoir=Frame(self.mainFrame,width=500,height=100)
        Label(self.framePouvoir,text="Glace").grid(row=0,column=0)
        self.b1=Button(self.framePouvoir,state = "disabled",disabledforeground = "GREY",text=str("Unlock niv. 2"),command=lambda:self.choisirPouvoir(1),width=10)
        self.b1.grid(row=1,column=0)
        Label(self.framePouvoir,text="Slash").grid(row=0,column=2)
        self.b2=Button(self.framePouvoir,state = "disabled",disabledforeground = "GREY",text=str("Unlock niv. 3"),command=lambda:self.choisirPouvoir(2),width=10)
        self.b2.grid(row=1,column=2)
        Label(self.framePouvoir,text="Vent").grid(row=0,column=4)
        self.b3=Button(self.framePouvoir,state = "disabled",disabledforeground = "GREY",text=str("Unlock niv. 4"),command=lambda:self.choisirPouvoir(3),width=10)
        self.b3.grid(row=1,column=4)
        Label(self.framePouvoir,text="Bombe",width=10).grid(row=0,column=6)
        self.b4=Button(self.framePouvoir,state = "disabled",disabledforeground = "GREY",text=str("Unlock niv. 5"),command=lambda:self.choisirPouvoir(4),width=10)
        self.b4.grid(row=1,column=6)
        Label(self.framePouvoir,text="Gu�rison",width=10).grid(row=0,column=8)
        self.b5=Button(self.framePouvoir,state = "disabled",disabledforeground = "GREY",text=str("Unlock niv. 5"),command=lambda:self.choisirPouvoir(5),width=10)
        self.b5.grid(row=1,column=8)
        
        
    def afficherMenuArgent(self):
        self.frameArgent=Frame(self.mainFrame,width=500,height=100)        
        l1=Label(self.frameArgent,foreground="DarkGoldenrod1", text="Argent = ")
        l1.pack(side=LEFT)
        l2=Label(self.frameArgent,textvariable=self.argent)
    
        l2.pack(side=LEFT)
        l7=Label(self.frameArgent,foreground="red", text="Vie = ")
        l7.pack(side=LEFT)
        l8=Label(self.frameArgent,textvariable=self.vie)
        l8.pack(side=LEFT)
        l5=Label(self.frameArgent,textvariable=self.vague)
        l5.pack(side=RIGHT)
        l6=Label(self.frameArgent,text="Vague = ")
        l6.pack(side=RIGHT)
        l4=Label(self.frameArgent,textvariable=self.nivo)
        l4.pack(side=RIGHT)
        l3=Label(self.frameArgent,text="Niveau = ")
        l3.pack(side=RIGHT)       
                 
        
    def afficherBarreVieJoueur(self):
        if not self.canevasCree:
            self.canevasVie = Canvas(self.frameArgent, bg='chartreuse2', width=100, height=12)
            
            self.canevasVie.pack(side=BOTTOM)
            self.canevasCree = 1
        if self.canevasCree:
            #pour determinier la couleur de la lifebar
            if int(self.vie.get()) * 5 ==100:
                couleurVie="chartreuse2"
            elif int(self.vie.get()) * 5/100 >= 0.9:
                couleurVie="lawn green"
            elif int(self.vie.get()) * 5/100 >= 0.8:
                couleurVie="green yellow"
            elif int(self.vie.get()) * 5/100 >= 0.7:
                couleurVie="OliveDrab1"
            elif int(self.vie.get()) * 5/100 >= 0.6:
                couleurVie="yellow3"
            elif int(self.vie.get()) * 5/100 >= 0.5:
                couleurVie="yellow"
            elif int(self.vie.get()) * 5/100 >= 0.4:
                couleurVie="gold2"
            elif int(self.vie.get()) * 5/100 >= 0.3:
                couleurVie="dark orange"
            elif int(self.vie.get()) * 5/100 >= 0.2:
                couleurVie="orange red"
            elif int(self.vie.get()) * 5/100 >= 0.1:
                couleurVie="OrangeRed2"
            elif int(self.vie.get()) * 5/100 >= 0:
                couleurVie="red2"
            else:
                couleurVie="black"
                
            self.canevasVie.config(width = self.parent.modele.vie * 5, bg=couleurVie) 
            self.canevasVie.delete("barreVieJoueur")
            self.canevasVie.create_text(self.canevasVie.winfo_reqwidth()/2, self.canevasVie.winfo_reqheight()/2, text=self.vie.get(), font=('verdana', 6, 'bold'), tags=("barreVieJoueur"))
            

       
        
    def afficherMenuTour(self):
        self.frameTour=Frame(self.mainFrame,width=100)
        self.afficherButtonTour()
        self.space1.pack(fill=BOTH) 
        self.frameTourFaible.pack(fill=BOTH)
        self.frameTourMoyenne.pack(fill=BOTH)
        self.frameTourForte.pack(fill=BOTH)
        self.space2.pack(fill=BOTH)
        self.frameUpgradeTourRange.pack(fill=BOTH)
        self.frameUpgradeTourForce.pack(fill=BOTH)
        self.frameUpgradeTourVitesse.pack(fill=BOTH)

                
    def afficherBouttonDemarrer(self):
        self.frameDemarrer=Frame(self.mainFrame,width=600,height=100)
        b=Button(self.frameDemarrer,text="Demarrer",command=self.parent.demarrePartie)
        b.pack()
        
    def afficherbuttonPause(self):
        self.framePause=Frame(self.mainFrame,width=600,height=100)
        b=Button(self.framePause,text="Pause",command=self.parent.pause)
        b.pack()
        b=Button(self.framePause,text="Recommencer",command=self.recommencer)
        b.pack()
        

    def activerTours(self):
            if self.parent.modele.cash >=75:              
                self.b6.config(state=NORMAL)
            else:
                self.b6.config(state=DISABLED)
            if self.parent.modele.cash >=300:             
                self.b7.config(state=NORMAL)
            else:
                self.b7.config(state=DISABLED)
            if self.parent.modele.cash >=1000:              
                self.b8.config(state=NORMAL)
            else:
                self.b8.config(state=DISABLED)

    def afficherButtonTour (self):
        self.frameTourFaible=Frame(self.frameTour,width=100,height=100)
        self.b6=Button(self.frameTourFaible,disabledforeground = "GREY",text=str(self.parent.modele.cout["Tourfaible"])+"$",command=lambda:self.choisirTour(1))
        self.b6.pack(side=BOTTOM,fill=BOTH) 
        Label(self.frameTourFaible,foreground="dark sea green",text="Tour faible").pack(side=BOTTOM)
        
        self.frameTourMoyenne=Frame(self.frameTour,width=100,height=100)
        self.b7=Button(self.frameTourMoyenne,disabledforeground = "GREY",text=str(self.parent.modele.cout["Tourmoyenne"])+"$",command=lambda:self.choisirTour(2))
        self.b7.pack(side=BOTTOM,fill=BOTH) 
        Label(self.frameTourMoyenne,foreground="medium sea green",text="Tour moyenne").pack(side=BOTTOM)
        
        self.frameTourForte=Frame(self.frameTour,width=100,height=100)
        self.b8=Button(self.frameTourForte,disabledforeground = "GREY",text=str(self.parent.modele.cout["Tourforte"])+"$",command=lambda:self.choisirTour(3))
        self.b8.pack(side=BOTTOM,fill=BOTH) 
        Label(self.frameTourForte,foreground="sea green",text="Tour forte").pack(side=BOTTOM)
        
        self.frameUpgradeTourRange=Frame(self.frameTour,width=100,height=100)
        b=Button(self.frameUpgradeTourRange,text=str(self.parent.modele.cout["UpgradeRange"])+"$",command=lambda:self.typeUpgrade(1))
        b.pack(side=BOTTOM,fill=BOTH)
        Label(self.frameUpgradeTourRange,text="Upgrade Range").pack(side=BOTTOM)
        
        self.frameUpgradeTourForce=Frame(self.frameTour,width=100,height=100)
        b=Button(self.frameUpgradeTourForce,text=str(self.parent.modele.cout["UpgradeForce"])+"$",command=lambda:self.typeUpgrade(2))
        b.pack(side=BOTTOM,fill=BOTH)
        Label(self.frameUpgradeTourForce,text="Upgrade Force").pack(side=BOTTOM)
        
        self.frameUpgradeTourVitesse=Frame(self.frameTour,width=100,height=100)
        b=Button(self.frameUpgradeTourVitesse,text=str(self.parent.modele.cout["UpgradeVitesse"])+"$",command=lambda:self.typeUpgrade(3))
        b.pack(side=BOTTOM,fill=BOTH)
        Label(self.frameUpgradeTourVitesse,text="Upgrade Vitesse").pack(side=BOTTOM)
        
        self.space1=Frame(self.frameTour,width=100,height=80)             
        self.space2=Frame(self.frameTour,width=100,height=80) 
         
    def afficheCreepProjectile(self):
        
        self.canevas.bind("<Motion>",self.afficheRange)
        self.canevas.bind("<Button-3>",self.parent.modele.nivoActif.resetTypeTour)
        self.canevas.bind("<KeyPress-Control_L>",self.parent.modele.nivoActif.holdCtrl)
        self.canevas.bind("<KeyRelease-Control_L>",self.parent.modele.nivoActif.releaseCtrl)
        self.canevas.delete("creep")
        self.canevas.delete("creep_life_bar")
        self.canevas.delete("projectile")
        
        for i in self.parent.modele.nivoActif.creepsEnCours:
            x1=i.pos[0]*5-(3*i.type/2+2)
            y1=i.pos[1]*5-(3*i.type/2+2)
            x2=i.pos[0]*5+(3*i.type/2+2)
            y2=i.pos[1]*5+(3*i.type/2+2)
            
            #dessine le creep
            self.canevas.create_oval(x1,y1,x2,y2, width=2, fill="olive drab", tags=("creep",))
            
            #pour determinier la couleur de la lifebar
            if i.vie/i.vieMax ==100:
                couleurVie="chartreuse2"
            elif i.vie/i.vieMax >= 0.9:
                couleurVie="lawn green"
            elif i.vie/i.vieMax >= 0.8:
                couleurVie="green yellow"
            elif i.vie/i.vieMax >= 0.7:
                couleurVie="OliveDrab1"
            elif i.vie/i.vieMax >= 0.6:
                couleurVie="yellow3"
            elif i.vie/i.vieMax >= 0.5:
                couleurVie="yellow"
            elif i.vie/i.vieMax >= 0.4:
                couleurVie="gold2"
            elif i.vie/i.vieMax >= 0.3:
                couleurVie="dark orange"
            elif i.vie/i.vieMax >= 0.2:
                couleurVie="orange red"
            elif i.vie/i.vieMax >= 0.1:
                couleurVie="OrangeRed2"
            elif i.vie/i.vieMax >= 0:
                couleurVie="red2"
            else:
                couleurVie="black"
            #dessine la lifebar    
            self.canevas.create_line(x1-3,y1-3,  x1+ (i.vie/i.vieMax)*10   ,y1-3, width=2, fill=couleurVie, tags=("creep_life_bar",))
      
        for i in self.parent.modele.nivoActif.projectile:
            
            x1=i.pos[0]*5-2
            y1=i.pos[1]*5-2
            x2=i.pos[0]*5+1
            y2=i.pos[1]*5+1
            
            self.canevas.create_oval(x1,y1,x2,y2,width=1,fill="white",tags=("projectile",))
        
        self.vie.set(str(self.parent.modele.vie))    
        self.argent.set(str(int(self.parent.modele.cash)))        
        self.nivo.set(str(self.parent.modele.nivo))
        self.vague.set(str(self.parent.modele.vague))
        self.debug.tempojeu.set("vitesse du jeu ("+str(self.parent.tempo)+")")
        if not self.parent.modele.nivoActif.tours:
             self.debug.rangetourfaible.set("Range Tour Faible (-)")
             self.debug.rangetourmoyenne.set("Range Tour Moyenne (-)")
             self.debug.rangetourforte.set("Range Tour Forte (-)")
             self.debug.vitessetourfaible.set("Vitesse Tour Faible (-)")
             self.debug.vitessetourmoyenne.set("Vitesse Tour Moyenne (-)")
             self.debug.vitessetourforte.set("Vitesse Tour Forte (-)")
             self.debug.forcetourfaible.set("Force Tour Faible(-)")
             self.debug.forcetourmoyenne.set("Force Tour Moyenne (-)")
             self.debug.forcetourforte.set("Force Tour Forte (-)")
        else:
            for i in range (len(self.parent.modele.nivoActif.tours)):
                if self.parent.modele.nivoActif.tours[i].type==1:
                    self.debug.rangetourfaible.set("Range Tour Faible ("+str(self.parent.modele.nivoActif.tours[i].range)+")")
                    self.debug.vitessetourfaible.set("Vitesse Tour Faible ("+str(self.parent.modele.nivoActif.tours[i].vitesse)+")")
                    self.debug.forcetourfaible.set("Force Tour Faible("+str(self.parent.modele.nivoActif.tours[i].force)+")")
                    
                if self.parent.modele.nivoActif.tours[i].type==2:
                    self.debug.rangetourmoyenne.set("Range Tour Moyenne ("+str(self.parent.modele.nivoActif.tours[i].range)+")")
                    self.debug.vitessetourmoyenne.set("Vitesse Tour Moyenne ("+str(self.parent.modele.nivoActif.tours[i].vitesse)+")")
                    self.debug.forcetourmoyenne.set("Force Tour Moyenne("+str(self.parent.modele.nivoActif.tours[i].force)+")")
                    
                if self.parent.modele.nivoActif.tours[i].type==3:
                    self.debug.rangetourforte.set("Range Tour Forte ("+str(self.parent.modele.nivoActif.tours[i].range)+")")
                    self.debug.vitessetourforte.set("Vitesse Tour Forte ("+str(self.parent.modele.nivoActif.tours[i].vitesse)+")")
                    self.debug.forcetourforte.set("Force Tour Forte("+str(self.parent.modele.nivoActif.tours[i].force)+")")
                
        self.afficherBarreVieJoueur()
		
     
    
    def creepMeurt(self,pos,type):  
        x=pos[0]*5
        y=pos[1]*5
        
        fG=((type-1)/2) + 1 #facteur de grosseur de la 'flaque'        
        self.canevas.create_oval(x-2*fG,y-2*fG,x+2*fG,y+2*fG,fill="red4", outline="red3", width=1, tags="sang")
        
        nbSplatters = random.randrange(2,8)
        
        for i in range(0,nbSplatters):
            
            x+=random.randrange(-2,2)*fG
            y+=random.randrange(-2,2)*fG
            
            randX=random.randrange(-4,4) + x
            randY=random.randrange(-4,4) + y
            randAdd=random.randrange(1,4)
            randWidth=random.randrange(0,randAdd) +1
            
            self.canevas.create_oval(randX,randY,randX+randAdd,randY+randAdd,fill="red4", outline="red3", width=randWidth, tags="sang")
       
        
     
    def afficheRange(self,evt):
        self.canevas.delete("range")
        x=evt.x
        y=evt.y
        type=self.parent.getType()
        range=self.parent.getRange([x,y],type)
        self.canevas.create_oval(range[0],range[1],range[0]+range[2],range[1]+range[2], width=2, outline="red" , tags=("range"))
        x=int(evt.x/5)
        y=int(evt.y/5)
       
    
    def recommencer(self):
        self.parent.pause=1
        self.root4=Toplevel(self.root)
        self.canvasrecommencer=Canvas(self.root4)
        framerecommencer=Frame(self.root4)
        framerecommencer.pack()
        b=Button(framerecommencer,text="Oui",command=self.recommencerJeu)
        b.pack(side=LEFT,fill=BOTH)
        b=Button(framerecommencer,text="Non",command=self.continuer)
        b.pack(side=RIGHT,fill=BOTH)
        labelrecommencer=Label(self.root4,text="Voulez-vous vraiment recommencer?")
        labelrecommencer.pack()


    def gameOver(self):
        self.root3.destroy()
        self.recommencerJeu()
              
    def recommencerJeu(self):
        self.root4.destroy()
        self.canevas.delete("tour")
        self.parent.recommencerPartie()
    
    def continuer(self):
        self.parent.pause=0
        self.root4.destroy()
        

    def affichePerdu(self):
        self.parent.mort=1
        self.root3=Toplevel(self.root)
        canvasPerdu = Canvas(self.root3,width=50,height=50)
        framePerdu=Frame(self.root3)
        framePerdu.pack()
        labelPerdu=Label(framePerdu,text="Vous avez perdu!")
        labelPerdu.pack()
        b=Button(framePerdu,text="Quitter",command=self.quitter)
        b.pack()
        b=Button(framePerdu,text="Recommencer",command=self.gameOver)
        b.pack()
        
    def quitter(self):
        self.parent.quitter()
    
    
class Debuger ():
    def __init__(self,parent):
        self.parent=parent
        self.vitesseJeu=0
        self.ptVieCreep=0
        self.vitesseCreep=0
        self.vitesseAttaquetourFaible=0
        self.vitesseAttaquetourMoyenne=0
        self.vitesseAttaquetourForte=0
        self.forceTourFaible=0
        self.forceTourMoyenne=0
        self.forceTourForte=0
        self.creepParNiveau=0
        self.rangeTourFaible=0
        self.rangeTourMoyennne=0
        self.rangeTourForte=0
        self.root2=Toplevel(self.parent.root)        
        self.f1=Frame(self.root2)        
        self.canevas=Canvas(self.root2,width=50,height=50)        
        self.canevas.pack()
        self.tempojeu=StringVar()        
        self.vitessetourfaible=StringVar()        
        self.vitessetourmoyenne=StringVar()        
        self.vitessetourforte=StringVar()        
        self.forcetourfaible=StringVar()        
        self.forcetourmoyenne=StringVar()        
        self.forcetourforte=StringVar()        
        self.rangetourfaible=StringVar()        
        self.rangetourmoyenne=StringVar()        
        self.rangetourforte=StringVar()        
        self.menuDebug()            
        
    def menuDebug(self):        
        lb=Label(self.f1,textvariable=self.tempojeu)        
        lb.pack()
        self.e1=Entry(self.f1)
        self.e1.pack()
        lb=Label(self.f1,text="pt de vie des creep")
        lb.pack()
        self.e2=Entry(self.f1)
        self.e2.pack()
        lb=Label(self.f1,text="vitesse des creeps")
        lb.pack()
        self.e3=Entry(self.f1)
        self.e3.pack()
        lb=Label(self.f1,textvariable=self.vitessetourfaible)
        lb.pack()
        self.e4=Entry(self.f1)
        self.e4.pack()
        lb=Label(self.f1,textvariable=self.vitessetourmoyenne)
        lb.pack()
        self.e9=Entry(self.f1)
        self.e9.pack()
        lb=Label(self.f1,textvariable=self.vitessetourforte)
        lb.pack()
        self.e10=Entry(self.f1)
        self.e10.pack()
        lb=Label(self.f1,textvariable=self.forcetourfaible)
        lb.pack()
        self.e5=Entry(self.f1)
        self.e5.pack()
        lb=Label(self.f1,textvariable=self.forcetourmoyenne)
        lb.pack()
        self.e7=Entry(self.f1)
        self.e7.pack()
        lb=Label(self.f1,textvariable=self.forcetourforte)
        lb.pack()
        self.e8=Entry(self.f1)
        self.e8.pack()
        lb=Label(self.f1,textvariable=self.rangetourfaible)        
        lb.pack()
        self.e11=Entry(self.f1)
        self.e11.pack()
        lb=Label(self.f1,textvariable=self.rangetourmoyenne)
        lb.pack()
        self.e12=Entry(self.f1)
        self.e12.pack()
        lb=Label(self.f1,textvariable=self.rangetourforte)
        lb.pack()
        self.e13=Entry(self.f1)
        self.e13.pack()
        lb=Label(self.f1,text="creep par niveau")
        lb.pack()
        self.e6=Entry(self.f1)
        self.e6.pack()
        self.f1.pack(side=TOP)
        b=Button(self.f1,text="Enter",command=self.enter)
        b.pack()

        
        
    def enter(self):
        self.vitesseJeu=self.e1.get()
        self.ptVieCreep=self.e2.get()
        self.vitesseCreep=self.e3.get()
        self.vitesseAttaquetourFaible=self.e4.get()
        self.vitesseAttaquetourMoyenne=self.e9.get()
        self.vitesseAttaquetourForte=self.e10.get()
        self.forceTourFaible=self.e5.get()
        self.forceTourMoyenne=self.e7.get()
        self.forceTourForte=self.e8.get()
        self.creepParNiveau=self.e6.get()
        self.rangeTourFaible=self.e11.get()
        self.rangeTourMoyennne=self.e12.get()
        self.rangeTourForte=self.e13.get()       
        self.attribuer()
        
        
    def attribuer(self):
        if self.vitesseJeu == "":
            pass
        else:
            self.parent.parent.tempo=self.vitesseJeu
        if self.creepParNiveau == "":
            pass
        else:
            self.parent.parent.modele.creepparnivo=int(self.creepParNiveau)
        if self.vitesseCreep == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.creeps:
                i.vitesse=float(self.vitesseCreep)
            for i in self.parent.parent.modele.nivoActif.creepsEnCours:
                i.vitesse=float(self.vitesseCreep)
        if self.ptVieCreep== "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.creeps:
                i.vie=int(self.ptVieCreep)
        if self.forceTourFaible == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==1:
                    i.force=int(self.forceTourFaible)
        if self.forceTourMoyenne == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==2:
                    i.force=int(self.forceTourMoyenne)
        if self.forceTourForte == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==3:
                    i.force=int(self.forceTourForte)                 
        if self.vitesseAttaquetourForte == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==3:
                    i.vitesse=int(self.vitesseAttaquetourForte)
        if self.vitesseAttaquetourFaible == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==1:
                    i.vitesse=int(self.vitesseAttaquetourFaible)
        if self.vitesseAttaquetourMoyenne == "":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==2:
                    i.vitesse=int(self.vitesseAttaquetourMoyenne)
        if self.rangeTourFaible =="":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==1:
                    i.range=int(self.rangeTourFaible)
        if self.rangeTourMoyennne=="":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==2:
                    i.range=int(self.vitesseAttaquetourMoyenne)
        if self.rangeTourForte=="":
            pass
        else:
            for i in self.parent.parent.modele.nivoActif.tours:
                if i.type==3:
                    i.range=int(self.rangeTourForte)
            
            
            
        
        
      
            