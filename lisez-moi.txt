Feast Of Creeps

Pour jouer, d�marrer l'application. Appuyer sur d�marrer pour commencer la partie.

Votre chateau est attaqu� par les Creeps, des ennemis terrifiants qui veulent manger tout le bois dont est fait votre chateau.
Pour d�fendre votre propri�t�, vous devrez utiliser des tours de d�fense.

Ces tours vous sont vendues � de modiques sommes et inclues tous les fraits de transport! 
Elles vous seront livr�es � l'instant o� vous informerez le manufacturier de l'endroit o� la positionner.

Il vous est possible d'am�liorer vos tours, mais cela requiert le travail d'un ouvrier sp�cialiste. 
Vous devrez donc avertir auparavant du type d'augmentation voulu et ensuite d�signer la tour � am�liorer.

Les dieux �tant avec vous dans votre mis�re, ils peuvent vous procurer un appuis significatif, mais les dieux ont un prix.
Les dieux n'aimant pas travailler pour rien, ils veulent s'assurer de la faisabilit� de votre r�ussite et donc se r�servent le
droit d'attendre de voir si vous �tes destin� � survivre ou mourrir avant de vous offrir certains pouvoirs. 
Faites vos preuves et les dieux vous seront coop�ratifs! Comme le veux le dictons: "Aides-toi et les dieux t'aideront" 

� vous de jouer!




Contr�les:	bouton gauche: 	mettre des tours apr�s avoir selectionn� le mod�le d�sir�
		bouton droit: 	annuler la s�lection de la tour
		ctrl:		tenir enfoncer le bouton permet de conserver la s�lection de la tour et donc de placer plusieurs tours 						d'un m�me type � la suite 
		