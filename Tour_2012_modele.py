# -*- coding: ISO-8859-1 -*-
'''

@author: Maxime Cot�, Jean-Philippe Parent, Marc-Andr� De Gagn�, Nicolas Waucheul, Shi-Hui Tran

Jeu de defense de tours
 les niveaux incremente la difficult�
    en augmentant la force des creeps

les tours peuvent b�n�ficier d'ameliorations
   en terme d'argent disponible
'''

import random
import math
import time


class Parcours():
    def __init__(self):
        self.noeuds=[[0,10],
                     [20,10],
                     [20,40],
                     [50,40],
                     [50,20],
                     [80,20],
                     [80,60],
                     [30,60],
                     [30,80],
                     [100,80]]

class TourFaible():
    def __init__(self,parent,pos):
        self.parent=parent
        self.type=1
        self.pos=pos
        self.cible=None
        self.vitesse=10
        self.compteurattaque=self.vitesse
        self.force=10
        self.range=10
        
        

class TourMoyenne():
    def __init__(self,parent,pos):
        self.parent=parent
        self.type=2
        self.pos=pos
        self.cible=None
        self.vitesse=25
        self.compteurattaque=self.vitesse
        self.force=25
        self.range=17.5

class TourForte():
    def __init__(self,parent,pos):
        self.parent=parent
        self.type=3
        self.pos=pos
        self.cible=None
        self.vitesse=50
        self.compteurattaque=self.vitesse
        self.force=500
        self.range=8
        
        
class Projectile():
    def __init__(self,parent,cible):
        self.parent=parent
        self.pos=parent.pos[:]
        self.cible=cible
        self.force=parent.force

class Pouvoir():
    def __init__(self, parent):
        self.parent = parent
        self.g=0
    
    def glace(self):
        

        for i in self.parent.nivoActif.creepsEnCours:
            i.vie -= i.vie/10
        
        self.g = 100 

    #Le slash enleve un quart de la vie aux creeps   
    def slash(self):
       
        
        for i in self.parent.nivoActif.creepsEnCours:
            i.vie = i.vie-i.vieMax/4 #Si un creep se fait toucher, son HP diminue d'un quart
    
        
    def vent(self):
       
        
        for i in self.parent.nivoActif.creepsEnCours:
            i.vitesse = i.vitesse *0.50 #Le vent relentit les creeps de 50%
            i.statut="r"
        
        
        self.parent.nivoActif.timervent=1
        
        
    #Elimine tous les creeps
    def nukebombe(self):
        
        
        for i in self.parent.nivoActif.creepsEnCours:
            i.vie = 0 
            #i.vie /=i.vie  si on veut faire qu'a la place ca leur laisse un pint de vie
            
        
    def heal(self, parent):
        if self.parent.vie<=15:
            self.parent.vie = self.parent.vie + 5 #Augmente le HP du joueur de 5 
        else:
            self.parent.vie=20
      
class Creep():
    def __init__(self,parent,type):
        self.parent=parent
        self.pos=self.parent.parcours.noeuds[0][:]
        self.cible=1 #indice du noeud de parcours a atteindre
        if self.pos[0]!=self.parent.parcours.noeuds[1][0]: # on simplifie le mouvement en verifiant uniquement l'axe de deplacement
            self.axe=0
            if self.pos[0]<self.parent.parcours.noeuds[1][0]:
                self.dir=1
            else:
                self.dir=-1
        else:
            self.axe=1
            if self.pos[1]<self.parent.parcours.noeuds[1][1]:
                self.dir=1
            else:
                self.dir=-1
        self.type = type           #force le type de creep, � envoyer en parametre � creep � terme.
        self.typeDeCreep(self)  #appel de type de creep
        
    def typeDeCreep(self,type):
        if self.type == 1:      #normal
            self.force = 1
            self.vitesse = 1/5
            self.vieMax = 0.05*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 13
            self.statut="n"
            
        if self.type == 2:      #creep rapide, peu de vie
            self.force = 1
            self.vitesse = 1/5
            self.vieMax = 0.1*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 13
            self.statut="n"
            
        if self.type == 3:      #creep lent, beaucoup de vie
            self.force = 2
            self.vitesse = 1/5
            self.vieMax = 0.15*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 13
            self.statut="n"
            
        if self.type == 4:      #creep normal
            self.force = 1
            self.vitesse = 1/5
            self.vieMax = 0.20*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 13
            self.statut="n"
            
        if self.type == 5:      #creep BOSS
            self.force = 5
            self.vitesse = 1/10
            self.vieMax = 0.4*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 12
            self.statut="n"
            
        if self.type == 6:      #creep BOSS 2
            self.force = 5
            self.vitesse = 1/10
            self.vieMax = 0.9*self.parent.constantevie
            self.vie = self.vieMax
            self.valeur = self.vieMax / 10
            self.statut="n"

                
        
    def bouge(self):
        #ajoute test pour changer de noeud
        #self.pos[self.axe]=self.pos[self.axe]+self.vitesse
        
        paxe=self.pos[self.axe]+(self.vitesse*self.dir)
        
        
        if self.dir==1:
            if paxe > self.parent.parcours.noeuds[self.cible][self.axe]:
                if self.cible<len(self.parent.parcours.noeuds)-1:
                    newpaxe=paxe-self.parent.parcours.noeuds[self.cible][self.axe]
                    self.pos=self.parent.parcours.noeuds[self.cible][:]
                    self.cible= self.cible+1
                    if self.axe==1:
                        self.axe=0
                        if self.pos[0]<self.parent.parcours.noeuds[self.cible][0]:
                            self.dir=1
                        else:
                            self.dir=-1
                    else:
                        self.axe=1
                        if self.pos[1]<self.parent.parcours.noeuds[self.cible][1]:
                            self.dir=1
                        else:
                            self.dir=-1
                else:
                    self.parent.parent.vie = self.parent.parent.vie -1
                    self.parent.creepsEnCours.remove(self)
            else:
                self.pos[self.axe]=paxe
        else:
            if paxe < self.parent.parcours.noeuds[self.cible][self.axe]:
                if self.cible<len(self.parent.parcours.noeuds)-1:
                    newpaxe=self.parent.parcours.noeuds[self.cible][self.axe]+paxe
                    self.pos=self.parent.parcours.noeuds[self.cible][:]
                    self.cible= self.cible+1
                    if self.axe==0:
                        self.axe=1
                        if self.pos[1]<self.parent.parcours.noeuds[self.cible][1]:
                            self.dir=1
                        else:
                            self.dir=-1
                    else:
                        self.axe=0
                        if self.pos[0]<self.parent.parcours.noeuds[self.cible][0]:
                            self.dir=1
                        else:
                            self.dir=-1
                else:
                    self.parent.creepsEnCours.remove(self)
            else:
                self.pos[self.axe]=paxe
        
class Nivo():
    def __init__(self,parent):
        self.parent=parent
        self.parcours = Parcours()
        self.largeurDuChemin=3        
        self.densiteCreep=3
        self.constantevie=300
        self.tours=[]
        self.creeps=[]
        self.creepsEnCours=[]
        self.listeObstacles = []  
        self.creeCreep()
        self.modeletourchoisie=0
        self.projectile=[]
        self.timervent = 0
        #self.pouvoir = Pouvoir(self)
        self.ctrlenfonce=0          
        
        
   #m�thode, retournant un bool�en, qui regarde si la position du "clique" est dans une positon valide (ex. hors du chemin) 
    def verifiePositionValidePourTour(self,pos):
        posX=pos[0]
        posY=pos[1]
        
        x=0 #pour que le code soit plus lisible, au lieu de faire pos[0][0] on peut faire pos[0][x]
        y=1
        noeuds = self.parcours.noeuds #encore une fois pour all�ger le code
        
        #v�rification pour le chemin
        for i in range(1,noeuds.__len__()):
            if noeuds[i-1][x] < noeuds[i][x] or noeuds[i-1][y] < noeuds[i][y]: #si le chemin va vers la droite ou le bas
                if int(posX) in range(noeuds[i-1][x] - self.largeurDuChemin - 2, noeuds[i][x] + self.largeurDuChemin + 2) and int(posY) in range(noeuds[i-1][y] - self.largeurDuChemin - 2, noeuds[i][y] + self.largeurDuChemin + 2):
                    return 0
            else: #si le chemin va vers la gauche ou vers le haut
                if int(posX) in range(noeuds[i][x] - self.largeurDuChemin - 2, noeuds[i-1][x] + self.largeurDuChemin + 2) and int(posY) in range(noeuds[i][y] - self.largeurDuChemin - 2, noeuds[i-1][y] + self.largeurDuChemin + 2):
                    return 0
        
        #v�rification sur un "obstacle"(arbre, roche, etc)
        largeurObstacle = 8
        for i in self.listeObstacles:
            if int(i[x]) in range(posX-largeurObstacle,posX+largeurObstacle) and int(i[y]) in range(posY-largeurObstacle,posY+largeurObstacle):
                   return 0
        #pour la grosse roche
        if posX in range(int(445/5),100) and posY in range(0,int(170/5)):
            return 0
        #pour le pont
        if posX in range(int(370/5),100) and posY in range(int(360/5),int(445/5)):
            return 0
        
        #v�rification sur une autre tour        
        if self.tours:
            for i in self.tours:               
                if int(i.pos[0]) in range(pos[0]-5,pos[0]+5) and int(i.pos[1]) in range(pos[1]-5,pos[1]+5):
                    return 0
        
        return 1
        
        
        
    def creeCreep(self):
        if self.parent.vague == 1:
            for i in range (0,10):
                self.creeps.append(Creep(self,1))
                
        elif self.parent.vague == 2:
            for i in range (0,10):
                self.creeps.append(Creep(self,1))
            for i in range (0,3):
                self.creeps.append(Creep(self,2))
                
        elif self.parent.vague == 3:
            for i in range (0,10):
                self.creeps.append(Creep(self,1))
            for i in range (0,3):
                self.creeps.append(Creep(self,2))
            for i in range (0,3):
                self.creeps.append(Creep(self,3))
                
        elif self.parent.vague == 4:
            for i in range (0,10):
                self.creeps.append(Creep(self,1))
            for i in range (0,3):
                self.creeps.append(Creep(self,2))
            for i in range (0,3):
                self.creeps.append(Creep(self,4))
           
                
        elif self.parent.vague == 5:
            for i in range (0,5):
                self.creeps.append(Creep(self,5))
            for i in range (0,1):
                self.creeps.append(Creep(self,6))
         
                
        elif self.parent.vague == 6:
            #quand c'est rendu a la derniere vague, on ecremente
            #le compteur de vagues
            self.parent.vague=0
            self.parent.nivo = self.parent.nivo+1
            self.constantevie=self.constantevie*1.5
            self.parent.parent.vue.canevas.delete("sang")#pour netoyyer le sang entre les niveaux... pas optimal par contre
            
            
    def bougeCreep(self):
        if self.creeps:
            ajoute=0
            c=self.creeps[0]
            if self.creepsEnCours:
                cPrecedent=self.creepsEnCours[0]
                if cPrecedent.cible==1: # onverifie si le dernier creep parti est assez loin seulement s'il est sur le m�me tron�on
                    if cPrecedent.pos[c.axe]>c.pos[c.axe]+c.parent.densiteCreep:
                        ajoute=1
            else:
                ajoute=1
            if ajoute:
                c=self.creeps.pop(0)
                c.pos=self.parcours.noeuds[0][:] # on positionne le creep sur le prmier noeud
                c.cible=1 #on vise le prochain noeud, le deuxieme
                self.creepsEnCours.insert(0,c)
        n=0
        if self.parent.pouvoir.g ==0:
            for i in self.creepsEnCours: 
                n=n+1
                i.bouge()
        else:
            self.parent.pouvoir.g = self.parent.pouvoir.g-1
            
    
    def setTour(self,pos):
        if self.modeletourchoisie==1:
            if self.parent.verificationArgent("Tourfaible"):
                self.parent.enleveArgent("Tourfaible")
                self.tours.append(TourFaible(self,pos))
                self.parent.parent.dessineTour(pos,self.modeletourchoisie)
        elif self.modeletourchoisie==2:
            if self.parent.verificationArgent("Tourmoyenne"):
                self.parent.enleveArgent("Tourmoyenne")
                self.tours.append(TourMoyenne(self,pos))
                self.parent.parent.dessineTour(pos,self.modeletourchoisie)
        elif self.modeletourchoisie==3:
            if self.parent.verificationArgent("Tourforte"):
                self.parent.enleveArgent("Tourforte")
                self.tours.append(TourForte(self,pos))
                self.parent.parent.dessineTour(pos,self.modeletourchoisie)
        
        if self.ctrlenfonce!=1:
            self.modeletourchoisie=0
            self.parent.parent.afficheRange()
            
    def VerifieCreepTouchable(self): 
        for i in self.tours:
            if i.compteurattaque%i.vitesse==0:
                posTourX=i.pos[0]
                posTourY=i.pos[1]
                
                #si la tour a une cible et qu'elle est encore dans son 
                #range, elle attaque
                if i.cible and math.fabs(posTourX-i.cible.pos[0])<i.range and  math.fabs(posTourY-i.cible.pos[1])<i.range :
                    self.CreeProjectile(i,i.cible)
                    i.compteurattaque=1 
                #sinon elle trouve le creep le plus proche et lattaque
                else :
                    hyp=0
                    hypmin=1000
                    for j in self.creepsEnCours:
                        posCreepX=j.pos[0]
                        posCreepY=j.pos[1]
                        diffX=math.fabs(posTourX-posCreepX)
                        diffY=math.fabs(posTourY-posCreepY)
                        hyp=diffX+diffY
                        if diffX<i.range and diffY<i.range and hyp<hypmin:
                            hypmin=hyp
                            i.cible=j
                            if i.type==3:
                                self.CreeProjectile(i, j)
                            
                            
                    #si on a trouv� une cible ds son range et que
                    #la tour n'est pas type 3, on attaque   
                    if i.cible and math.fabs(posTourX-i.cible.pos[0])<i.range and  math.fabs(posTourY-i.cible.pos[1])<i.range and i.type!=3:
                        self.CreeProjectile(i,i.cible)
                    
                    i.compteurattaque=1 
            else:
                i.compteurattaque=i.compteurattaque+1
                                     
       
    def CreeProjectile(self,tour,creep):
        self.projectile.append(Projectile(tour,creep))
        
    def BougeProjectile(self):
        for i in self.projectile:
            if i.cible==None:
                if 0>i.pos[0]>100 or 0>i.pos[1]>100:
                    self.projectile.remove(i)
                else:
                    if i.pos[0]<i.parent.pos[0]:
                        i.pos[0]=i.pos[0]-1
                    elif i.pos[0]>i.parent.pos[0]:
                        i.pos[0]=i.pos[0]+1
                    if i.pos[1]<i.parent.pos[1]:
                        i.pos[1]=i.pos[1]-1
                    elif i.pos[1]>i.parent.pos[1]:
                        i.pos[1]=i.pos[1]+1
            else:
                if i.pos[0]<i.cible.pos[0]:
                    i.pos[0]=i.pos[0]+1
                if i.pos[0]>i.cible.pos[0]:
                    i.pos[0]=i.pos[0]-1
                if i.pos[1]<i.cible.pos[1]:
                    i.pos[1]=i.pos[1]+1
                if i.pos[1]>i.cible.pos[1]:
                    i.pos[1]=i.pos[1]-1

                
                
    def VerifieProjectileToucheCible(self):
        for i in self.projectile:
            if i.cible!=None:
                if math.fabs(i.pos[0]-i.cible.pos[0])<=1:
                    if math.fabs(i.pos[1]-i.cible.pos[1])<=1:
                       i.cible.vie-=i.force
                       self.projectile.remove(i)
                     
                   
                   
                    
    def VerifieCreepsMorts(self):

        self.creepsARemover=[]
        
        for i in self.creepsEnCours:
            if i.vie<=0:
                self.creepsARemover.append(i)
        
        for i in self.creepsARemover:
            
            for j in self.projectile:
                if j.cible==i:
                    j.cible=None
                    break
            for k in self.tours:
                if k.cible==i:
                    k.cible=None
            if i in self.creepsEnCours:
                self.parent.cash=self.parent.cash + i.valeur
                self.creepsEnCours.remove(i)
                self.parent.parent.vue.creepMeurt(i.pos,i.type)
    
    def VerifiePouvoirVentTermine(self):
         if self.timervent==0:
             pass
         elif self.timervent==300:
             for i in self.creepsEnCours:
                 if i.statut=="r":
                     i.vitesse=i.vitesse*2
                     i.statut="n"
                     self.timervent=0
         elif self.timervent<300:
             self.timervent+=1
             
        
        
                
    def resetTypeTour(self,evt):
        self.modeletourchoisie=0
        
    def holdCtrl(self,evt):
        self.ctrlenfonce=1
    
    def releaseCtrl(self,evt):
        self.ctrlenfonce=0
        self.modeletourchoisie=0
        self.parent.parent.deleteRange()
    
    def creeListeObstacles(self):
        #cr�ation de la liste des obstacles        
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("sapin1"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("sapin2"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("sapin3"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("sapin4"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("roche1"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("roche2"))
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("roche3")) 
        self.listeObstacles.append(self.parent.parent.vue.canevas.coords("roche3"))
        #r�ajustement des coordonn�es pour travailler dans notre grille interne de 100x100
        for i in self.listeObstacles:
            i[0]=i[0]/5
            i[1]=i[1]/5   
            
            
class Modele():
    def __init__(self, parent):
        self.parent=parent
        self.vie=20
        self.cash=300
        self.pouvoir = Pouvoir(self)
        self.nivo=0
        self.vague=1
        self.tempsentrelesvagues=300
        self.typeupgrade=0
        self.timervent=0
        self.cout={"Tourfaible":75,"Tourmoyenne":300,"Tourforte":1000,
                   "Glace":300,"Slash":500,"Vent":300,"Nuke":1000,"Heal":1200,
                   "UpgradeRange":300,"UpgradeForce":300,"UpgradeVitesse":300}
    
    
    def getTypeTour(self,pos):
        for i in self.nivoActif.tours:               
            if int(i.pos[0]) in range(pos[0]-2,pos[0]+2) and int(i.pos[1]) in range(pos[1]-2,pos[1]+2):
                return i.type
        
    
    def calculerRange(self,pos,type):
        x=pos[0]
        y=pos[1]
        if type!=0:       
            if type==1:
                x=(x-(100/2))
                y=(y-(100/2))
                radius=100
            elif type==2:
                x=(x-200/2)
                y=(y-200/2)
                radius=200
            elif type==3:
                x=(x-80/2)
                y=(y-80/2)
                radius=80
                
            range=[x,y,radius]
        else:
            range=[0,0,0]
        
        return range
    
    
    def recommencerPartie(self):
        self.cash=300
        self.vie=20
        self.nivo=1
        self.vague=0
        self.nivoActif.tours=[]
        self.nivoActif.creeps=[]
        self.nivoActif.creepsEnCours=[]
        self.nivoActif.modeletourchoisie=0
        self.nivoActif.projectile=[]
        self.nivoActif.ctrlenfonce=0
        
    
    
    def choisirTypeTour(self,type):
        if self.nivo==0:
            pass
        else:
            if type==1:
                if self.verificationArgent("Tourfaible"):
                    self.nivoActif.modeletourchoisie=type
            elif type==2:
                if self.verificationArgent("Tourmoyenne"):
                    self.nivoActif.modeletourchoisie=type
            elif type==3:
                if self.verificationArgent("Tourforte"):
                    self.nivoActif.modeletourchoisie=type
            
    def typeUpgrade(self,type):
        self.typeupgrade=type
        
  
    def choisirTypePouvoir(self,type):
        if self.nivo >0:
            if type==1:
                if(self.verificationArgent("Glace")):
                    self.enleveArgent("Glace")
                    self.pouvoir.glace()
        if self.nivo > 0:
            if type==2:
                if(self.verificationArgent("Slash")):
                    self.enleveArgent("Slash")
                    self.pouvoir.slash()
        if self.nivo > 0:
            if type==3:
                self.compteur=0
                for i in self.nivoActif.creepsEnCours:
                    if i.statut=="r":
                        self.compteur+=1
                        break
                if self.compteur==0:
                    if(self.verificationArgent("Vent")):
                            self.enleveArgent("Vent")
                            self.pouvoir.vent()
        if self.nivo > 0:
            if type==4:
                if(self.verificationArgent("Nuke")):
                    self.enleveArgent("Nuke")
                    self.pouvoir.nukebombe()
            else:
                if(self.verificationArgent("Heal")):
                    self.enleveArgent("Heal")
                    self.pouvoir.heal(self)
            
            
        
    def demarrePartie(self):
        self.nivo=self.nivo+1
        self.nivoActif=Nivo(self)
        
        
    def setTour(self,pos):
        #r�ajustement des coordonn�es pour travailler dans notre grille interne de 100x100
        pos[0]=int(5* round(float(pos[0])/5))#arondissement de la valeur pour simuler l'effet d'une grille
        pos[1]=int(5* round(float(pos[1])/5))        
        pos[0]=int(pos[0]/5)
        pos[1]=int(pos[1]/5)
        
        if self.nivoActif.verifiePositionValidePourTour(pos):
            self.nivoActif.setTour(pos)
        
     
   
     
     
    def upgradeTour(self,pos):
        if self.nivoActif.tours:
            for i in self.nivoActif.tours:               
                if int(i.pos[0]) in range(pos[0]-2,pos[0]+2) and int(i.pos[1]) in range(pos[1]-2,pos[1]+2):
                    if self.typeupgrade==1:
                        if self.verificationArgent("UpgradeRange"):
                            self.enleveArgent("UpgradeRange")
                            i.range=int(i.range/4+i.range)
                    if self.typeupgrade==2:
                        if self.verificationArgent("UpgradeForce"):
                            self.enleveArgent("UpgradeForce")
                            i.force=int(i.force/4+i.force)
                    if self.typeupgrade==3:
                        if i.vitesse >5:
                            if self.verificationArgent("UpgradeVitesse"):
                                self.enleveArgent("UpgradeVitesse")
                                i.vitesse=int(i.vitesse-i.vitesse/4)
                        
            

                    
          
         
        
    def verificationArgent(self,objet):
        if self.cout[objet]>self.cash:
            return 0
       
        return 1
        
    def enleveArgent(self,objet):
        self.cash=self.cash-self.cout[objet]
        

if __name__ == '__main__':
    m=Modele(1)
    m.demarrePartie()
