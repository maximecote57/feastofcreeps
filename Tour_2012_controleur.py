# -*- coding: ISO-8859-1 -*-
'''
@author: Maxime Cot�, Jean-Philippe Parent, Marc-Andr� De Gagn�, Nicolas Waucheul, Shi-Hui Tran
'''
import Tour_2012_modele as mod
import Tour_2012_vue as vue

class Controleur():
    def __init__(self):
        self.tempo=20
        self.modele=mod.Modele(self)
        self.vue=vue.Vue(self)
        self.compteurnivo=1
        self.pause=0
        self.mort=0
     
    def recommencerPartie(self):
        self.modele.recommencerPartie()
        self.pause=0
        self.mort=0
        
    def quitter(self):
        self.vue.root.destroy()
         
        
    def demarrePartie(self):
        self.vue.affichermenuPause()   
        self.modele.demarrePartie()
        self.vue.afficheModele()
        self.modele.nivoActif.creeListeObstacles()
        self.continuePartie()
        
    def pause(self):
        if self.pause==0:
            self.pause=1
            self.vue.status=0
        else:
            self.pause=0
            self.vue.status=1
    

     
            
    def continuePartie(self):
        if self.pause==1 or self.mort==1:
            self.vue.root.after(self.tempo,self.continuePartie)
        else:
            if self.modele.vie >0:
                if len(self.modele.nivoActif.creepsEnCours) == 0:
                    if self.compteurnivo%self.modele.tempsentrelesvagues==0:
                        self.modele.vague=self.modele.vague+1
                        self.modele.nivoActif.creeCreep()
                        self.compteurnivo=1
                        self.vue.activerPouvoirs()
                    else:
                        self.compteurnivo=self.compteurnivo+1
                    
                
                self.modele.nivoActif.bougeCreep()
                self.vue.afficheCreepProjectile()
                self.modele.nivoActif.VerifieCreepTouchable()
                self.vue.activerTours()     
                self.modele.nivoActif.BougeProjectile()
                self.modele.nivoActif.VerifieProjectileToucheCible()
                self.modele.nivoActif.VerifieCreepsMorts()
                self.modele.nivoActif.VerifiePouvoirVentTermine()
                self.vue.root.after(self.tempo,self.continuePartie)
            else:
                self.vue.affichePerdu()
                self.vue.root.after(self.tempo,self.continuePartie)
     
    def choisirTypeTour(self,type):
        if self.pause==0:
            self.modele.choisirTypeTour(type)
        
    def choisirTypePouvoir(self,type):
        if self.pause==0:
            self.modele.choisirTypePouvoir(type)
        
    def typeUpgrade(self,type):
        if self.pause==0:
            self.modele.typeUpgrade(type) 
            
    def setTour(self,pos):
        self.modele.setTour(pos)
    
    def upgradeTour(self,pos):
        self.modele.upgradeTour(pos)
        
    def dessineTour(self,pos,type):
        self.vue.dessineTour(pos,type)
    
    def afficheRange(self):
        self.vue.afficheRange
    
    def deleteRange(self):
        self.vue.canevas.delete("range")
        
    def getRange(self,pos,type):
        return self.modele.calculerRange(pos,type)

    def getType(self):
        return self.modele.nivoActif.modeletourchoisie


        

if __name__ == '__main__':
    print("Initialise l'application")
    cont=Controleur()
    print("Affiche la fen�tre d'ouverture")
    print("\nBon jeu! Amusez-vous bien!\n\n\nFait par: \nMaxime Cot�,\nJean-Philippe Parent, \nMarc-Andr� De Gagn�, \nNicolas Waucheul, \nShi-Hui Tran")
    cont.vue.root.mainloop()
